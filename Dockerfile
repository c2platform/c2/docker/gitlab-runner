FROM ubuntu:22.04

ARG PYENV=c2d
ENV PYENV=${PYENV}

SHELL ["/bin/bash", "-c"]

RUN apt update \
    && apt install virtualenv curl wget docker.io -y \
    && mkdir ~/.virtualenv && \
    DEB_PYTHON_INSTALL_LAYOUT='deb' virtualenv ~/.virtualenv/$PYENV -p python3

RUN mkdir -p /etc/docker/certs.d/gitlab.c2platform.org/ \
    && mkdir -p /etc/docker/certs.d/registry.c2platform.org/ \
    && cd /etc/docker/certs.d/gitlab.c2platform.org/ \
    && curl -o ca.crt "https://gitlab.com/c2platform/ansible/-/raw/master/.ca/c2/c2.crt" \
    && cd /etc/docker/certs.d/registry.c2platform.org/ \
    && curl -o ca.crt "https://gitlab.com/c2platform/ansible/-/raw/master/.ca/c2/c2.crt"

RUN source ~/.virtualenv/$PYENV/bin/activate && \
    pip3 install ansible-core==2.15.0 \
        ansible-builder==3.0.0

# Copy the entrypoint script into the container
COPY entrypoint.sh /entrypoint.sh

# Make the entrypoint script executable
RUN chmod +x /entrypoint.sh

# Set the entrypoint script as the default entrypoint
ENTRYPOINT ["/entrypoint.sh"]

# By default, run a bash shell.
CMD ["/bin/bash"]
