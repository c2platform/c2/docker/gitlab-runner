# GitLab Runner Image for Ansible Execution Environments

[![pipeline status](https://gitlab.com/c2platform/c2/docker/gitlab-runner/badges/master/pipeline.svg)](https://gitlab.com/c2platform/c2/docker/gitlab-runner/-/commits/master)
[![Latest Release](https://gitlab.com/c2platform/c2/docker/gitlab-runner/-/badges/release.svg)](https://gitlab.com/c2platform/c2/docker/gitlab-runner/-/releases)

This project provides a Dockerized GitLab Runner environment specifically
designed for creating and managing Ansible Execution Environments within a CI/CD
pipeline. Based on Ubuntu 22.04, it includes all necessary tools for Ansible
automation, linting, and execution environment creation, all encapsulated within
a Python virtual environment. Refer to
[Manage the RWS Ansible Execution Environment | C2 Platform](https://c2platform.org/docs/howto/rws/exe-env)
for more information.

## Features

* Ubuntu 22.04 Base: A stable and widely-used base for Docker images.
* Pre-installed Ansible Tools: Comes with ansible-core, yamllint, ansible-lint,
and ansible-builder, ready for automation tasks. 8Integrated Python Virtual
Environment: Isolates Python dependencies to avoid conflicts and ensure
reproducible builds.
* Docker-in-Docker Capability: Enables the creation and management of Docker
  containers and images within CI/CD jobs.
* Secure Certificate Handling: Includes certificates for gitlab.c2platform.org
  and registry.c2platform.org to ensure secure connections within CI/CD
  pipelines.
* Customizable Entrypoint: Facilitates the use of the container with an
  entrypoint script that activates the Python virtual environment and passes
  commands dynamically.

## Prerequisites

* Docker installed on your host machine or CI/CD runner.
* Access to a GitLab instance for running CI/CD pipelines.

## Building the Docker Image

Navigate to the directory containing the `Dockerfile` and execute:

```bash
export IMAGE_NAME="gitlab-runner"
export IMAGE_VERSION="0.0.1"
export IMAGE="$IMAGE_NAME:$IMAGE_VERSION"
docker build -t $IMAGE . | tee build.log
docker run -it --rm $IMAGE .
```

Example

```bash
(c2) vagrant@c2d-xtop:~/images/gitlab-runner$ docker run -it --rm $IMAGE
root@2f3b45e29a2e:/# ansible --version
ansible [core 2.15.0]
  config file = None
  configured module search path = ['/root/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /root/.virtualenv/c2d/lib/python3.10/site-packages/ansible
  ansible collection location = /root/.ansible/collections:/usr/share/ansible/collections
  executable location = /root/.virtualenv/c2d/bin/ansible
  python version = 3.10.12 (main, Nov 20 2023, 15:14:05) [GCC 11.4.0] (/root/.virtualenv/c2d/bin/python)
  jinja version = 3.1.3
  libyaml = True
root@2f3b45e29a2e:/# ansible-builder --version
3.0.0
root@2f3b45e29a2e:/# git --version
git version 2.34.1
root@2f3b45e29a2e:/# exit
exit
```
