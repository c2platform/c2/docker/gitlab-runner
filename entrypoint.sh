#!/bin/bash

# Activate the virtual environment
source ~/.virtualenv/$PYENV/bin/activate

# Execute the command passed to the docker run command
exec "$@"
